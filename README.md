# README

## A Tiny Node Server to Test Expanse LLC Web OAuth Flows for Identity Providers and AWS Lambda Authorizer ##

## References
[AWS APIGateway Documentation](http://docs.aws.amazon.com/apigateway/latest/developerguide/use-custom-authorizer.html)

## Sister Projects

* [identity_provider_web](https://bitbucket.org/meghane/identity_provider_web)
* [authorizer](https://bitbucket.org/meghane/authorizer)

## Summary
Client auth code is in [identity_provider_web](https://bitbucket.org/meghane/identity_provider_web). The **run.sh** script will automatically clone **identity_provider_web** and configure this tiny node server so the **below URI paths work**:

`GET http://localhost:8081`

`GET http://localhost:8081/google`

`GET http://localhost:8081/fb`

## Set Up
Just Run The Bash Script

```bash
$ cd { $projectDir }
$ sh run.sh
```

## Prerequisites
 + bash
 + npm
 + node
 + git
 + identity_provider_web repo access

## Git Authentication Errors
If you receive git authentication errors, follow this [guide](https://git-scm.com/docs/gitcredentials) to configure git to cache credentials.

Otherwise you can configure git to prompt you for your password:
`$ git config --global core.askPass`

## Local override
You can specify a local version of [identity_provider_web](https://bitbucket.org/meghane/identity_provider_web) if you would like as well.

```bash
$ run.sh {path_to_identity_provider_web}
```
