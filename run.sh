#!/usr/bin/env bash
#
PROJECT_NAME="tiny_node_server"

clear
echo "Executing Tiny Node Server Runner"
echo "Please check the Readme for dependencies (npm, node, repo access, etc) . . ."

if [ $1 ]; then
  echo "Using override path to identity_provider_web"
  LOCAL_REPO="$1"
else
  #WORKING_DIR=$(find / -iname "$PROJECT_NAME" 2>/dev/null)
  WORKING_DIR=$(echo $0 | sed s/run.sh//g)
  cd $WORKING_DIR

  CURRENT_DIR=$(pwd)
  ORIGIN_REPO="https://bitbucket.org/meghane/identity_provider_web.git"
  LOCAL_REPO="$CURRENT_DIR/tiny"

  # if the local repo exists, remove it to reclone
  if [ -d "$LOCAL_REPO" ]; then
    echo "Removing $LOCAL_REPO as it already exists . . ."
    rm -rf "$LOCAL_REPO"
  fi;

  echo "Locating Origin $ORIGIN_REPO"

  git clone "$ORIGIN_REPO" "$LOCAL_REPO"
  if [ $? -ne 0 ]; then
    echo "There was an issue cloning $ORIGIN_REPO. Verify access to the repo. Exiting now . . ."
    exit
  fi
fi

echo "Setting global environment variables for the dispatcher . . ."
export GOOGLE_FILE_PATH="$LOCAL_REPO/google/google.html"
export FB_FILE_PATH="$LOCAL_REPO/facebook/facebook.html"
export AMZN_FILE_PATH="$LOCAL_REPO/amazon/amazon.html"

echo "GOOGLE_FILE_PATH has been set to $GOOGLE_FILE_PATH"
echo "FB_FILE_PATH has been set to $FB_FILE_PATH"
echo "AMZN_FILE_PATH has been set to $AMZN_FILE_PATH";

echo "Installing required node modules via npm . . ."
npm install

if [ $? -ne 0 ]; then
  echo "There was an issue installing the node packages via npm. Check your npm installation . . ."
  exit
fi;

echo "Starting the tiny node server . . ."
node index.js
