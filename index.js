'use strict';
const logger = require('winston');
const http = require('http');
const fs = require('fs');
const env = require('node-env-file');
const dispatcher = require('httpdispatcher');
const PORT = 8081;
const googfileUri = process.env.GOOGLE_FILE_PATH;
const fbfileUri = process.env.FB_FILE_PATH;
const amznfileUri = process.env.AMZN_FILE_PATH;

http.createServer((req, res) => {
  dispatcher.dispatch(req, res);
}).listen(PORT);

logger.info(`Server is now running on port ${PORT}`);

dispatcher.onGet('/', (req, res) => {
  res.writeHeader(200, {
    'Content-Type': 'text/html'
  });
  res.write('Check the projects README for the URI Paths');
  res.end();
});

dispatcher.onGet('/google', (req, res) => {
  fs.readFile(googfileUri, (err, data) => {
    if (err) {
      throw err;
    }
    res.writeHeader(200, {
      'Content-Type': 'text/html'
    });
    res.write(data);
    res.end();
  });
});

dispatcher.onGet('/facebook', (req, res) => {
  fs.readFile(fbfileUri, (err, data) => {
    if (err) {
      throw err;
    }
    res.writeHeader(200, {
      'Content-Type': 'text/html'
    });
    res.write(data);
    res.end();
  });
});

dispatcher.onGet('/amazon', (req, res) => {
  fs.readFile(amznfileUri, (err, data) => {
    if (err) {
      throw err;
    }
    res.writeHeader(200, {
      'Content-Type': 'text/html'
    });
    res.write(data);
    res.end();
  });
});
